import 'package:flutter/material.dart';

void showToast(BuildContext context, {required String message, required Color backgroundColor}) {
  final snackBar = SnackBar(
    content: Text(
      message,
      style:
      const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    ),
    backgroundColor: backgroundColor,
    duration: const Duration(milliseconds: 650),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}