import 'package:crud/models/task_model.dart';
import 'package:flutter/material.dart';

import '../screens/add_or_update_task.dart';

class TaskRendered extends StatelessWidget {
  const TaskRendered({Key? key, required this.task, required this.index, required this.navigateToEditPage, required this.deleteById}) : super(key: key);

  final TaskModel task;
  final int index;
  final Function(TaskModel) navigateToEditPage;
  final Function(String id) deleteById;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => showTaskInfo(context, task),
      leading: CircleAvatar(
        backgroundColor: Colors.blueGrey,
        child: Text(
          '${index + 1}',
          style: const TextStyle(color: Colors.white),
        ),
      ),
      title: Text(task.title),
      subtitle: Text(task.description),
      trailing: PopupMenuButton(
        onSelected: (value) {
          if (value == 'edit') {
            navigateToEditPage(task);
          } else if (value == 'delete') {
            deleteById(task.sId!);
          }
        },
        itemBuilder: (context) {
          return [
            const PopupMenuItem(value: 'edit', child: Text('Edit')),
            const PopupMenuItem(value: 'delete',child: Text('Delete')),
          ];
        },
      ),
    );
  }


  void showTaskInfo(BuildContext context, TaskModel task) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      context: context,
      isScrollControlled: true,
      builder: (context) {
        return SingleChildScrollView(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(task.title, style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              const SizedBox(height: 16,),
              SizedBox(
                  height: 50,
                  child: Text(task.description, maxLines: 15, style: const TextStyle(fontSize: 18),)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddOrUpdateTodoPage(task: task),));
                    // Navigator.pop(context);
                  }, child: const Text('Update task')),
                  TextButton(onPressed: () {
                    Navigator.pop(context);
                  }, child: const Text('Cancel'))
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
