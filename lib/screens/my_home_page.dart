import 'package:crud/models/task_model.dart';
import 'package:crud/screens/add_or_update_task.dart';
import 'package:crud/services/todo_services.dart';
import 'package:flutter/material.dart';

import '../widgets/task.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<TaskModel> tasksList = [];
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final shouldPop = await showQuitDialog();
        return shouldPop ?? false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Rest API Todo app'),
        ),
        body: Visibility(
          visible: isLoading,
          replacement: Visibility(
            visible: tasksList.isNotEmpty,
            replacement: const Center(
                child: Text(
              'Your task here',
              style: TextStyle(fontSize: 24),
            )),
            child: buildTasksList(),
          ),
          child: const Center(child: CircularProgressIndicator()),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: navigateToAddPage,
          tooltip: 'Increment',
          label: const Text('Add Task'),
        ),
      ),
    );
  }

  Widget buildTasksList() {
    return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
            child: RefreshIndicator(
              onRefresh: onRefresh,
              child: ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    final task = tasksList[index];
                    return Card(child: TaskRendered(task: task, index: index, deleteById: deleteById, navigateToEditPage: navigateToEditPage,));
                  },
                  separatorBuilder: (context, index) => const Divider(
                        height: 4,
                        color: Colors.white,
                      ),
                  itemCount: tasksList.length),
            ),
          );
  }

  Future<bool?> showQuitDialog() {
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: const Text('Do you want to quit?'),
        actions: [
          ElevatedButton(onPressed: () {
            Navigator.pop(context, true);
          }, child: const Text('Quit')),
          TextButton(onPressed: () {
            Navigator.pop(context, false);
          }, child: const Text('Cancel'))
        ],
      );
    },);
  }


  Future onRefresh() async {
    setState(() {
      isLoading = true;
      tasksList.clear();
    });
    fetchData();
  }


  Future<void> navigateToAddPage() async {
    final route = MaterialPageRoute(
      builder: (context) => AddOrUpdateTodoPage(),
    );
    await Navigator.push(context, route);
    setState(() {
      tasksList.clear();
      isLoading = true;
    });
    fetchData();
  }

  Future<void> navigateToEditPage(TaskModel task) async {
    final route = MaterialPageRoute(
      builder: (context) => AddOrUpdateTodoPage(task: task),
    );
    await Navigator.push(context, route);
    setState(() {
      tasksList.clear();
      isLoading = true;
    });
    fetchData();
  }

  Future<void> deleteById(String id) async {
   final isSuccess = await TodoService.deleteTaskById(id);
    if (isSuccess) {
      final filteredList = tasksList.where((task) => task.sId != id).toList();
      setState(() {
        tasksList = filteredList;
      });
    } else {
      throw Exception('Error');
    }
  }



  Future<List<TaskModel>> fetchData() async {
    final response = await TodoService.fetchTasks();
    if (response.isNotEmpty) {
      setState(() {
        isLoading = false;
        tasksList = response;
      });
    } else {
      throw Exception('Error');
    }
    return tasksList;
  }
}
