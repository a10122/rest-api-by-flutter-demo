import 'package:crud/screens/my_home_page.dart';
import 'package:crud/services/todo_services.dart';
import 'package:flutter/material.dart';

import '../models/task_model.dart';
import '../utils/toast_snackbar.dart';

// ignore: must_be_immutable
class AddOrUpdateTodoPage extends StatefulWidget {
  AddOrUpdateTodoPage({Key? key, this.task}) : super(key: key);
  TaskModel? task;

  @override
  State<AddOrUpdateTodoPage> createState() => _AddOrUpdateTodoPage();
}

class _AddOrUpdateTodoPage extends State<AddOrUpdateTodoPage> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  bool isEdit = false;

  @override
  void initState() {
    super.initState();
    final task = widget.task;
    if (task != null) {
      isEdit = true;
      final title = task.title;
      titleController.text = title;
      final description = task.description;
      descriptionController.text = description;
    }
  }

  @override
  void dispose() {
    super.dispose();
    titleController.dispose();
    descriptionController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(isEdit ? 'Edit Task' : 'Add Task'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          TextField(
            controller: titleController,
            decoration: const InputDecoration(hintText: 'Title'),
          ),
          TextField(
            controller: descriptionController,
            decoration: const InputDecoration(
              hintText: 'Description',
            ),
            keyboardType: TextInputType.multiline,
            minLines: 5,
            maxLines: 8,
          ),
          const SizedBox(
            height: 30,
          ),
          ElevatedButton(
              onPressed: () {
                isEdit ? updateData() : submitData();
              },
              style:
                  ElevatedButton.styleFrom(padding: const EdgeInsets.all(10)),
              child: Text(isEdit ? 'Submit edit' : 'Add new task',
                  style: const TextStyle(fontSize: 22)))
        ],
      ),
    );
  }

  Future<void> updateData() async {
    final task = widget.task;
    if (task == null) return;
    final title = titleController.text;
    final description = descriptionController.text;
    final body = TaskModel(
      sId: task.sId,
      title: title,
      description: description,
    );
    final response = await TodoService.updateTask(body);
    //  show success/ fail message after submitted
    if (response && mounted) {
      showToast(context,
          message: 'Updated success', backgroundColor: Colors.green);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => const MyHomePage(),
          ),
          (route) => false);
    } else {
      showToast(context, message: 'Error', backgroundColor: Colors.red);
    }
  }

  Future<void> submitData() async {
    //  Get data from forms and submit to server
    final title = titleController.text;
    final description = descriptionController.text;
    final task = TaskModel(
      title: title,
      description: description,
    );
    final response = await TodoService.addTask(task);
    //  show success/ fail message after submitted
    if (response && mounted) {
      showToast(context, message: 'Success', backgroundColor: Colors.green);
      Navigator.pop(context);
    } else {
      showToast(context, message: 'Error', backgroundColor: Colors.red);
    }
  }
}
