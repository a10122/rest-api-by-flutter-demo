class TaskModel {
  String? sId;
  final String title;
  final String description;
  bool? isCompleted;

  TaskModel(
      {this.sId, required this.title, required this.description, this.isCompleted}) {
    isCompleted = isCompleted ?? false;
  }

  factory TaskModel.fromJson(Map<String, dynamic> json) {
    return TaskModel(
        sId: json['_id'],
        title: json['title'],
        description: json['description'],
        isCompleted: json['is_completed']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['title'] = title;
    data['description'] = description;
    data['is_completed'] = isCompleted;
    return data;
  }
}
