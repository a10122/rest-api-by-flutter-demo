import 'dart:convert';
import 'package:crud/models/task_model.dart';
import 'package:http/http.dart' as http;

class TodoService {

  static Future<List<TaskModel>> fetchTasks() async {
    final List<TaskModel> tasksList = [];
    const url = 'https://api.nstack.in/v1/todos?page=1&limit=10';
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      final responseBody = jsonDecode(response.body);
      responseBody['items'].forEach((json) {
        tasksList.add(TaskModel.fromJson(json));
      });
      return tasksList;
    } else {
      throw Exception('error');
    }
  }

  static Future<bool> addTask(TaskModel task) async {
    final body = TaskModel(
      title: task.title,
      description: task.description,
    );
    final json = body.toJson();
    const url = 'https://api.nstack.in/v1/todos';
    final uri = Uri.parse(url);
    final response = await http.post(uri,
        body: jsonEncode(json), headers: {'Content-Type': 'application/json'});
    return response.statusCode == 201;
  }

  static Future<bool> updateTask(TaskModel task) async {
    final json = task.toJson();
    String url = 'https://api.nstack.in/v1/todos/${task.sId}';
    final uri = Uri.parse(url);
    final response = await http.put(uri,
        body: jsonEncode(json), headers: {'Content-Type': 'application/json'});
    return response.statusCode == 200;
  }

  static Future<bool> deleteTaskById(String id) async {
    final url = 'https://api.nstack.in/v1/todos/$id';
    final uri = Uri.parse(url);
    final response = await http.delete(uri);
    return response.statusCode == 200;
  }

}